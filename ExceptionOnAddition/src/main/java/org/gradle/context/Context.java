package org.gradle.context;

import java.util.ArrayList;
import org.gradle.exception.UserDefinedException;

public class Context {
	
	public ArrayList<UserDefinedException> exceptionList=new ArrayList<UserDefinedException>();

	public ArrayList<UserDefinedException> getExceptionList() {
		return exceptionList;
	}

	public void setExceptionList(UserDefinedException e) {
		this.exceptionList.add(e);
	}

}
