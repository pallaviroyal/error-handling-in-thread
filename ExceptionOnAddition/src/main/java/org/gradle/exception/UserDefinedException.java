package org.gradle.exception;

import java.util.ArrayList;

public class UserDefinedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
    public UserDefinedException( )
	{
		super();
	}
	public UserDefinedException(String msg)
	{
		super(msg);
	}


}
