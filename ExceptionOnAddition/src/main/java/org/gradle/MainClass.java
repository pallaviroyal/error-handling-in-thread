package org.gradle;

import java.util.Iterator;

import org.gradle.container.MyThreadLocal;
import org.gradle.context.Context;
import org.gradle.exception.UserDefinedException;

public class MainClass {

	public static void main(String[] args) {
		
		Addition add=new Addition();
		Context context=new Context();
		MyThreadLocal.set(context);
	       Integer a=10;
	       Integer b=20;
	       Integer result = null;
	       try
	       {
	       result= add.addition(a, b);
	       }
	       catch(UserDefinedException e)
	       {
	    	 Context contextNew=MyThreadLocal.get();
	    	 Iterator<UserDefinedException> iterate=contextNew.exceptionList.iterator();
	    	 System.out.println("Exceptions List:");
	    	 while(iterate.hasNext())
	    	 {
	    		 System.out.println(iterate.next());
	    	 }
	       }
	       catch(Exception e)
	       {
	    	 Context contextNew=MyThreadLocal.get();
	    	 Iterator<UserDefinedException> iterate=contextNew.exceptionList.iterator();
	    	 System.out.println("Exceptions List:");
	    	 while(iterate.hasNext())
	    	 {
	    		 System.out.println(iterate.next());
	    	 }
	       }
	       
	       System.out.println(result);
	      
	      
	}

}
