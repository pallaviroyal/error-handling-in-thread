package org.gradle;

import org.gradle.container.MyThreadLocal;
import org.gradle.context.Context;
import org.gradle.exception.UserDefinedException;

public class Addition {
	
	public Integer addition(Integer a,Integer b) 
	{
		Context context=MyThreadLocal.get();
		if(a==null)
		{
			context.setExceptionList(new UserDefinedException("Variable 'a' (Integer type) getting null value"));
		}
		if(b==null)
		{
			context.setExceptionList(new UserDefinedException("Variable 'b' (Integer type) getting null value"));
		}
		
	MyThreadLocal.set(context);
		return a+b;
	}
	public Integer multiplication(Integer a,Integer b) throws Exception 
	{
		
		Integer c=a*b;
		return c;
	}

}
